def convert_dec_to_base(x, base):
    num_in_new_base = list()

    while x > 0:
        num_in_new_base.append(x % base)
        x = int(x / base)

    num_in_new_base.reverse()

    return num_in_new_base


if __name__ == '__main__':
    try:
        x_decimal = int(eval(input("x: ")))
        base = int(input("New base: "))
        result_list = convert_dec_to_base(x_decimal, base)
        result_txt = ''.join(map(lambda val: str(val), result_list))
        print(f'Result: {result_txt}')
    except Exception as e:
        raise e
