import unittest

import main


class TestConvertToBase(unittest.TestCase):

    def test_convert_to_base(self):
        self.assertEqual(main.convert_dec_to_base(10, 2), [1, 0, 1, 0])
        self.assertEqual(main.convert_dec_to_base(10, 8), [1, 2])
        self.assertEqual(main.convert_dec_to_base(10, 10), [1, 0])
        self.assertEqual(main.convert_dec_to_base(10, 7), [1, 3])
        self.assertEqual(main.convert_dec_to_base(12445, 8), [3, 0, 2, 3, 5])
        self.assertEqual(main.convert_dec_to_base(978574, 16), [14, 14, 14, 8, 14])
        res = main.convert_dec_to_base(2 ** 1000, 7)
        self.assertEqual(res[len(res) - 2:len(res):], [1, 2])


if __name__ == '__main__':
    unittest.main()
